package com.okola.mtekdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class Main2Activity extends AppCompatActivity {
    public ImageButton imageButton4;
    public ImageButton imageButton6;
    public ImageButton imageButton5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ImageButton imageButton4 = (ImageButton) findViewById(R.id.imageButton4);
        imageButton4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity_main6();
            }
        });
        ImageButton imageButton6 = (ImageButton) findViewById(R.id.imageButton6);
        imageButton6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity_main7();
            }
        });
        ImageButton imageButton5 = (ImageButton) findViewById(R.id.imageButton5);
        imageButton5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity_main8();
            }
        });
    }

    public void openActivity_main6() {
        Intent intent = new Intent(this, Main6Activity.class);
        startActivity(intent);
    }

    public void openActivity_main7() {
        Intent intent = new Intent(this, Main7Activity.class);
        startActivity(intent);
    }

    public void openActivity_main8() {
        Intent intent = new Intent(this, Main8Activity.class);
        startActivity(intent);

    }
}
