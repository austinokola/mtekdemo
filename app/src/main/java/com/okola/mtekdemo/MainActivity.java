package com.okola.mtekdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button button2;
    private ImageButton imageButton;
    private ImageButton imageButton2;
    private ImageButton imageButton3;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"You have to signup first!",Toast.LENGTH_LONG).show();
            }
        });

    imageButton3 = (ImageButton) findViewById(R.id.imageButton3);
    imageButton3.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            openActivity_main5();
        }
    });

    imageButton2 = (ImageButton)  findViewById(R.id.imageButton2);
    imageButton2.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            openActivity_main4();
        }
    });
    imageButton = (ImageButton)    findViewById(R.id.imageButton);
    imageButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            openActivity_main3();
        }
    });

    button2 = (Button) findViewById(R.id.button2);
    button2.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) { openActivity_main2();


        }
    });

    }
    public void  openActivity_main2(){
        Intent intent = new Intent(this, Main2Activity.class);
        startActivity(intent);
    }
    public void openActivity_main3(){

        Intent intent = new Intent(this,Main3Activity.class );
        startActivity(intent);
    }
    public void openActivity_main4(){

        Intent intent = new Intent(this,Main4Activity.class );
        startActivity(intent);
    }
    public void openActivity_main5(){

        Intent intent = new Intent(this,Main5Activity.class);
        startActivity(intent);
    }
}
