package com.okola.mtekdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class Main7Activity extends AppCompatActivity {
    public Button button3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main7);
        Button button3 = (Button) findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity_bottom_navigation();
            }
        });

    }
    public void openActivity_bottom_navigation(){
        Intent intent = new Intent(this,botttom_navigation.class);
        startActivity(intent);
    }
}
